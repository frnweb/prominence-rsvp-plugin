import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './app.js';
import Form from './form.js';

import "./assets/styles/main.scss";

if ( document.getElementById('app') ) {
    ReactDOM.render( <App />, document.getElementById('app'));
} else if ( document.getElementById('marketo_form') ) {
    ReactDOM.render(<Form />, document.getElementById('marketo_form'));
}