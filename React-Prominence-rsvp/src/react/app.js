import React, { useEffect, useState } from 'react';
import axios from 'axios'

import Filters from './components/filters';
import List from './components/List';
import withListLoading from './components/withListLoading';

function App() {

    const ListLoading = withListLoading(List);
    const [appState, setAppState] = useState({
      loading: false,
      seminars: null,
    });
  
    useEffect(() => {
      setAppState({ loading: true });
      const parameter = new URLSearchParams(location.search).get("state")

      const apiUrl = parameter !== null ? `https://rsvp.myprominencehealthplan.com/api/seminars?state=${parameter}` : 'https://rsvp.myprominencehealthplan.com/api/seminars';

      axios.get(apiUrl).then((seminars) => {
        const allSeminars = seminars.data;
        setAppState({ loading: false, seminars: allSeminars });
      });
    }, [setAppState]);

      return (
        <div className='App'>
          <Filters setAppState = {setAppState} />
          <ListLoading isLoading={appState.loading} seminars={appState.seminars} />
        </div>
      )
}

export default App;
