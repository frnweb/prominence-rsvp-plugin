import React, { useEffect, useState } from 'react';

function Form() {

    useEffect(() => {
        const script = document.createElement('script');      
        script.src = "//go.prominencehealthplan.com/js/forms2/js/forms2.min.js";
        script.defer = true;
      
        document.body.appendChild(script);

        var search = window.location.search;
        var params = new URLSearchParams(search);

        MktoForms2.loadForm("//go.prominencehealthplan.com", "204-LAX-877", 1173, function(form){
            form.vals({
                "eventID": params.get('id'),
                "eventName": params.get('title'),
                "eventType": params.get('type'),
                "eventLocation": params.get('location'),
                "eventZipCode": params.get('zip'),
                "eventDate": params.get('date'),
                "eventStartTime": params.get('time')
            });

        });
                    
    }, []);

    return (
        <div className='Form'>
            <div className='form-container'>
                <form id="mktoForm_1173"></form>
            </div>
        </div>
    )
}

export default Form;


