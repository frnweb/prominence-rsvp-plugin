import React from 'react';
import Moment from 'react-moment';
import moment from 'moment';


const List = (props) => {
  const { seminars } = props;

  if (!seminars || seminars.length === 0) return <p>There are more seminars on the way!<br />
  To learn about the Prominence plan in your area, <br />please call
  us at 800-992-9023 (TTY: 711).</p>;
  return (
    <div class="rsvp_list">
      <h2>Events</h2>
      <p>Learn more about our Medicare Advantage Plans. Find a date and time that’s convenient for you and register today!</p>
      {seminars.map((seminar) => {
        let [digits] = seminar.startDate.match(/\d+/)
        let link = `/seminar-registration?title=${seminar.title}&type=${seminar.typeName}&id=${seminar.id}&location=${seminar.venue.address1} ${seminar.venue.city}, ${seminar.venue.state} &zip=${seminar.venue.zip}&date=${moment.unix(digits / 1000).format("MM/DD/YYYY")}&time=${moment.unix(digits / 1000).format("MM/DD/YYYY h:mm A")}`
        return (
        <div className="rsvp_list__seminars">
          <div className="rsvp_list__card">
            <div className="rsvp_row">

              <div className="rsvp_cell rsvp_list__date small-12 medium-2 large-2">
                <h3 className="rsvp_list__month">{formatDate( seminar.startDate, 'month')}</h3>
                <h3 className="rsvp_list__day">{formatDate(seminar.startDate, 'day')}</h3>
              </div>

              <div className="rsvp_cell rsvp_list__info small-12 medium-5 large-5">
                <h3>{ seminar.title }</h3>
                <p>
                  { seminar.venue.name } <br />
                  { seminar.venue.address1 } <br />
                  { seminar.venue.city },
                  { seminar.venue.state }
                  { seminar.venue.zip }
                </p>
              </div>

              <div className="rsvp_cell rsvp_list__schedule small-12 medium-2 large-2">
                  <p>{formatDate( seminar.startDate, 'schedule')}</p>
              </div>

              <div className="rsvp_cell small-12 medium-3 large-3">
                <a className="rsvp_button" href={link}>Register Now</a>
              </div>

            </div>
          </div>
        </div>);
      })}
    </div>
  );


  function formatDate(startDate, type){
    const [digits] = startDate.match(/\d+/)
    if (type === 'month'){
      var format = 'MMM'
    } else if (type === 'day') {
      var format = 'D'
    } else if (type === 'schedule') {
      var format = 'dddd[\n]MMM D, YYYY[\n]h:mm a'
    } else {
      var format = 'MMM D'
    }

    return (
      <Moment style={{whiteSpace: "pre"}} unix format={format}>{digits / 1000}</Moment>
    )
  }
};

export default List;