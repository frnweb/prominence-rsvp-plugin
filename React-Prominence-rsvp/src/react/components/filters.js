import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios'
import qs from 'qs'

const Filters = (props) => {

    const [filterState, setFilterState] = useState({
        types: null,
      });
    
    const typeUrl = 'https://rsvp.myprominencehealthplan.com/api/seminarTypes';
    axios.get(typeUrl).then((types) => {
        const allTypes = types.data
        setFilterState({ types: allTypes });
    });

    const zipInput = useRef(null);
    const distanceInput = useRef(null);
    const typeInput = useRef(null);

  return (
    <div className="rsvp_filters">
        <h2>Find an Event in Your Area and Register Today!</h2>
        <p>Prominence Medicare hosts informational events and seminars in your area, plus classes for members. Learn all about Medicare and Medicare Advantage, as well as other topics to get the most out of your plan and stay on top of your health.</p>
        <div className="rsvp_filters__input">
            <div className="rsvp_row">
              <div className="rsvp_cell small-12 medium-4 large-4">
                <label>Location</label>
                <input ref={zipInput} id="rsvp_zip_search"></input>
              </div>
              <div className="rsvp_cell small-12 medium-4 large-4">
                <label>Maximum Distance</label>
                <select ref={distanceInput} name="eventDistance" id="eventDistance">
                    <option value="none">None Selected</option>
                    <option value="25">25 Miles</option>
                    <option value="50">50 Miles</option>
                    <option value="100">100 Miles</option>
                    <option value="500">500 Miles</option>
                </select>
              </div>
              <div className="rsvp_cell small-12 medium-4 large-4">
                <label>Type of Event</label>
                <select ref={typeInput} name="eventType" id="eventType">
                    <option value="none">None Selected</option>
                    {filterState.types !== null ? filterState.types.map((type) => {
                        return (
                            <option key={type.id} value={type.id}>{type.name}</option>
                        );
                    }) : null }
                </select>
              </div>
              {/* <div className="small-12 medium-3 large-3">
                <p>Do you prefer an in-person or virtual event?</p>
                <input type="radio" id="in-person" name="type" value="In-person event" checked></input> <label for="in-person">In-person event</label>
                <input type="radio" id="virtual" name="type" value="Virtual event"></input><label for="virtual">Virtual event</label>
                <input type="radio" id="either" name="type" value="Either is okay"></input><label for="either">Either is okay</label>
              </div> */}
            </div>
            <button onClick={submitAPI} className="rsvp_button">Find an event</button>

        </div>
    </div>

  );

  function submitAPI() {

    const zipInputValue = zipInput !== null ? zipInput.current.value : '';

    const distanceInputValue = distanceInput.current.value !== 'none' ? distanceInput.current.value : '';

    const typeInputValue = typeInput.current.value !== 'none' ? typeInput.current.value : '';

    const url = `https://rsvp.myprominencehealthplan.com/api/seminars?zip=${zipInputValue}&radius=${distanceInputValue}&typeName=${typeInputValue}`

    axios.get(url).then((seminars) => {
        const allSeminars = seminars.data;
        props.setAppState({ loading: false, seminars: allSeminars });
      });
  }

};

export default Filters;