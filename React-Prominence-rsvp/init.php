<?php 

// include the Composer autoload file
require 'vendor/autoload.php';

wp_enqueue_script( 'wm_react', plugin_dir_url( __FILE__ ) . '/build/react.js', array( 'jquery' ), null, true );



function agentcubed_rsvp_function( ) {

  
    return '<div id="app"></div>';

}

add_shortcode('rsvp_shortcode', 'agentcubed_rsvp_function');


function marketo_form_function( ) {

    return '<script src="//go.prominencehealthplan.com/js/forms2/js/forms2.min.js"></script><div id="marketo_form"></div>';

}

add_shortcode('marketo_rsvp_form', 'marketo_form_function');
